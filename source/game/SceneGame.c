
#include <sys/types.h>

#include "config.h"
#include "framebuffer.h"
#include "mt.h"
#include "timer.h"
#include "gpioctrl.h"

#include "EnemySpawner.h"
#include "DrawText.h"
#include "SpriteManager.h"
#include "Ship.h"
#include "ShipBullet.h"
#include "EnemySpawner.h"

void SceneGame_checkInput();

int finished;
uint64_t nextShoot;

void
SceneGame_init() {
	SpriteManager_init();
	Ship_init();
	ShipBullet_init();
	EnemySpawner_init();
	finished = 0;
	nextShoot = 0;
}

void
SceneGame_loop() {

	SceneGame_checkInput();
	EnemySpawner_update();
	ShipBullet_update();
	Ship_update();
	finished = EnemySpawner_enemyCount() > 50;

	uint8_t *fb = framebuffer_getptr();
	framebuffer_clear();
	SpriteManager_draw(fb);
	DrawNumber(fb, 0, 5, 1, timer_fps_current);
	framebuffer_commit();
	timer_count_frame();
}

void
SceneGame_checkInput() {
	if ( Ship_posY() > 10 && gpio_pin20() ) Ship_moveUp();
	if ( Ship_posY() < 470 && gpio_pin21() ) Ship_moveDown();
	if ( gpio_pin16() && timer_current() > nextShoot ) {
		Ship_shoot();
		nextShoot = timer_current() + BULLET_SHOOT_TIME;
	}
}

int
SceneGame_finished() {
	return finished;
}
