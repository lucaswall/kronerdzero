
#include <sys/types.h>

#include "gpioctrl.h"

#define ARM_IO_BASE		0x3F000000
#define ARM_GPIO_BASE		(ARM_IO_BASE + 0x200000)
#define ARM_GPIO_GPPUD		(ARM_GPIO_BASE + 0x94)
#define ARM_GPIO_GPPUDCLK0	(ARM_GPIO_BASE + 0x98)
#define ARM_GPIO_GPLEV0		(ARM_GPIO_BASE + 0x34)

#define MODE_PULLUP 2
#define WAIT_SETUP_TIME 200

volatile uint32_t *gpio_gppud = (uint32_t *) ARM_GPIO_GPPUD;
volatile uint32_t *gpio_gppudclk0 = (uint32_t *) ARM_GPIO_GPPUDCLK0;
volatile uint32_t *gpio_gplev0 = (uint32_t *) ARM_GPIO_GPLEV0;

void wait_some_time(uint32_t cycles);

void
gpio_init() {
	*gpio_gppud = MODE_PULLUP;
	wait_some_time(WAIT_SETUP_TIME);
	*gpio_gppudclk0 = 0;
	*gpio_gppudclk0 |= 1 << 16;
	*gpio_gppudclk0 |= 1 << 20;
	*gpio_gppudclk0 |= 1 << 21;
	wait_some_time(WAIT_SETUP_TIME);
	*gpio_gppud = 0;
	*gpio_gppudclk0 = 0;
}

int
gpio_pin16() {
	return (*gpio_gplev0 & (1 << 16)) == 0;
}

int
gpio_pin20() {
	return (*gpio_gplev0 & (1 << 20)) == 0;
}

int
gpio_pin21() {
	return (*gpio_gplev0 & (1 << 21)) == 0;
}
